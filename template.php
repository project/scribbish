<?php
function scribbish_regions() {
  return array(
    'right' => t('right sidebar'),
    'content' => t('content'),
    'footer' => t('footer')
  );
}

function scribbish_comment($comment, $links = array()) { 
  $output  = '<div class="comment'. ($comment->status == COMMENT_NOT_PUBLISHED ? ' comment-unpublished' : '') .'">'; 
  $output .= '<div class="author"><cite>'.theme('username', $comment).'</cite> - <abbr title="'.format_date($comment->timestamp).'">'.format_date($comment->timestamp).'</abbr></div>';
  
 // $output .= '<div class="subject">'. l($comment->subject, $_GET['q'], NULL, NULL, "comment-$comment->cid") .' '. theme('mark', $comment->new) ."</div>\n"; 
  $output .= '<div class="content">'. $comment->comment .'</div>'; 
  $output .= '<div class="links">'. theme('links', $links) .'</div>'; 
  $output .= '</div>'; 
  return $output; 
}

function scribbish_trackback($trackback, $display_links = TRUE) {
  $output  = '<div class="trackback" id="trackback-'. $trackback->trid ."\">\n";
 $output .= '<div class="author">';
 $output .= '<a href="'. $trackback->url .'">'. $trackback->subject ."</a>\n";
 $output .= '<abbr>From <cite>'.$trackback->name.'</cite></abbr>';
 $output .= '</div>';
// $output .= '<cite>'.$trackback->name.'</cite> - <abbr title="'.format_date($trackback->created).'">'.format_date($trackback->created).'</abbr></div>';
  $output .= '<div class="content">'. check_markup($trackback->excerpt) ."</div>\n";
  if ($display_links && (user_access('administer trackbacks') || node_access('update', node_load($trackback->nid)))) {
    $output .= '<div class="links">';
    $links = array();
    $links['trackback_edit'] = array(
      'title' => t('edit'),
      'href' => "admin/content/trackback/edit/$trackback->trid");
    $links['trackback_delete'] = array(
      'title' => t('delete'),
      'href' => "admin/content/trackback/delete/$trackback->trid");
    if (module_exists('spam')) {
      $links = array_merge($links, trackback_spam_link($trackback));
    }
    $output .= theme('links', $links);
    $output .= "</div>\n";
  }
  $output .= "</div>\n";
  return $output;
}

function scribbish_links($links, $attributes = array('class' => 'links')) {
  $output = '';
  $tl = array();
  
  if (count($links) > 0) { 
    $num_links = count($links); 
    $i = 1; 

    $where = 'links';

    foreach ($links as $key => $link) { 
      if ($key == 'node_read_more') {
        continue;
      }
      
      if ($i == 1 && strpos($key, 'taxonomy_term_') !== false) {
        $where = 'tag';
      }
      
      $class = ''; 

      // Automatically add a class to each link and also to each LI 
      if (isset($link['attributes']) && isset($link['attributes']['class'])) { 
        $link['attributes']['class'] .= ' ' . $key; 
        $class = $key; 
      } 
      else { 
        $link['attributes']['class'] = $key; 
        $class = $key; 
      } 

      // Add first and last classes to the list of links to help out themers. 
      $extra_class = ''; 
      if ($i == 1) { 
        $extra_class .= 'first '; 
      } 
      if ($i == $num_links) { 
        $extra_class .= 'last '; 
      } 
      // Is the title HTML? 
      $html = isset($link['html']) && $link['html']; 

      // Initialize fragment and query variables. 
      $link['query'] = isset($link['query']) ? $link['query'] : NULL; 
      $link['fragment'] = isset($link['fragment']) ? $link['fragment'] : NULL; 

      if (isset($link['href'])) { 
        $tl[] = l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment'], FALSE, $html); 
      } 
      else if ($link['title']) { 
        //Some links are actually not links, but we wrap these in <span> for adding title and class attributes 
        if (!$html) { 
          $link['title'] = check_plain($link['title']); 
        } 
        $tl[] = '<span'. drupal_attributes($link['attributes']) .'>'. $link['title'] .'</span>'; 
      } 
      $i++; 
    } 
  } 
  
  if ($where == 'links') {
    $output = implode(', ', $tl);
  }
  else {
    $output .= implode(' ', $tl);
  }


  return $output;
}

/**
 * Sets the body-tag class attribute.
 *
 * Adds 'sidebar-left', 'sidebar-right' or 'sidebars' classes as needed.
 */
function phptemplate_body_class($sidebar_right) {
  if ($sidebar_right != '') {
      $class = 'sidebar-right';
  }

  if (isset($class)) {
    print ' class="'. $class .'"';
  }
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return '<div id="comments">'. $content . '</div>';
  }
  else {
    return '<div id="comments"><h3 class="comments">'. t('Comments') .'</h3>'. $content .'</div>';
  }
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function _phptemplate_variables($hook, $vars) {
  if ($hook == 'page') {

    if ($secondary = menu_secondary_local_tasks()) {
      $output = '<span class="clear"></span>';
      $output .= "<ul class=\"tabs secondary\">\n". $secondary ."</ul>\n";
      $vars['tabs2'] = $output;
    }

    // Hook into color.module
    if (module_exists('color')) {
      _color_page_alter($vars);
    }
    return $vars;
  }
  return array();
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= "<ul class=\"tabs primary\">\n". $primary ."</ul>\n";
  }

  return $output;
}

<div class='sidebar-node block-<?php print $block->module ?>' id='block-<?php print $block->module .'-'. $block->delta; ?>'>
  <?php if ($block->subject): ?>
    <h3><?php print $block->subject ?></h3>
  <?php endif;?>
  <?php print $block->content ?>
</div>
<div class="hentry" id="article-<?php print $node->nid ?>">
  <?php if ($page == 0): ?>
    <h2 class="entry-title node-title"><a href="<?php print $node_url ?>" rel="bookmark" title="Permanent Link to <?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>
  <div class="vcard">
    Posted by <span class="fn"><?php print $node->name; ?></span>
  </div>
  <abbr class="published" title="<?php print format_date($node->created)?>"><?php print format_date($node->created); ?></abbr>
  <br class="clear" />
  <div class="entry-content">
    <?php print $content; ?>
    <?php if ($page == 0): ?>
    <div class="extended">
      <p><a href="<?php print $node_url; ?>"><?php print t('Continue reading...') ?></a></p>
    </div>
    <?php endif; ?>
  </div>
  <ul class="meta">
    <?php if ($taxonomy): ?>
    <li>Tags:
    <?php print $terms ?>
    </li>
    <? endif; ?>
    <li>Meta:
      <?php if ($links): ?>
        <?php 
         print $links;
         print ', ';
        ?>
      <?php endif; ?>      
      <a href="<?php print $node_url; ?>" rel="bookmark">permalink</a>
    </li>
  </ul>
</div>

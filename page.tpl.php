<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
<head profile="http://gmpg.org/xfn/11">
  <title><?php print $head_title ?></title>
	<?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <style type="text/css" media="all">
    @import url(<?php print base_path().path_to_theme()?>/stylesheets/application.css);
  </style>
</head>

<body>
<div id="container">
  <div id="header">
    <h1><span><a href="<?php print check_url($base_path); ?>" title="<?php print $site_name; ?>"><?php print $site_name; ?></a></span></h1>
    <?php if ($site_slogan) { ?>
    <h2><?php print $site_slogan; ?></h2>
    <?php } ?>
  </div>

  <div id="page">
    <div id="content" class="hfeed">
      <?php if ($breadcrumb): print '<div class="navigation">'. $breadcrumb .'<div class="clear"></div></div>'; endif; ?>
      <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
      <?php if ($title && !$is_front): print '<h1 class="entry-title title'. ($tabs ? ' with-tabs' : '') .'">'. $title .'</h1>'; endif; ?>
      <?php if ($tabs): print $tabs .'</div>'; endif; ?>
      <?php if (isset($tabs2)): print $tabs2; endif; ?>
      <?php if ($help): print $help; endif; ?>
      <?php if ($messages): print $messages; endif; ?>
      <?php print $content ?>
    </div>

    <div id="sidebar">
      <!-- search -->
      <?php if (!$sidebar_left && $search_box): ?><div class="sidebar-node"><?php print $search_box ?></div><?php endif; ?>

      <!-- sidebar components -->
      <?php if (isset($primary_links)) : ?>
      <div class="sidebar-node block-block" id="block-block-1">
        <h3><?php print t('Menu') ?></h3>
        <ul>
          <?php
            foreach ($primary_links as $link) {
              print '<li><a href="'.url($link['href']).'">'.$link['title'].'</a></li>';
            }
          ?>
        </ul>
      </div>
      <?php endif; ?>
      <?php if ($sidebar_right): ?>
      <?php print $sidebar_right ?>
      <?php endif; ?>
    </div>
    <br style="clear:both;" />
  </div>

  <div id="footer">
    <hr />
    <p><a href="<?php print check_url($base_path); ?>" title="<?php print $site_name; ?>"><?php print $site_name; ?></a></p>
    <?php if ($footer_message): print '<p>'.$footer_message.'</p>'; endif; ?>
    <ul>
      <li>powered by <a href='http://www.drupal.org'>drupal</a> /
          styled with <a href='http://quotedprintable.com/pages/scribbish'>scribbish</a> /
          ported by <a href='http://www.voidberg.org'>alexandru badiu</a>
          </li>
    </ul>
  </div>
</div>
</body>
</html>